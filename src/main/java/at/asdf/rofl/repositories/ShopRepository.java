package at.asdf.rofl.repositories;

import at.asdf.rofl.domain.Shops;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Joyce on 13.01.2015.
 */
@Repository
public interface ShopRepository extends CrudRepository<Shops, Integer>{
}
